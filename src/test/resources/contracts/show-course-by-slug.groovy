import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        urlPath("/courses/java-soa-integration")

        method GET()

        headers {
            accept applicationJson()
        }
    }

    response {
        status OK()

        headers {
            contentType applicationJson()
        }

        body(
                "id": "java-soa-integration",
                "title": "Java Soa Integration",
                "description": "Some description",
                "level": "ADVANCED",
                "category": "PROGRAMMING"
        )
    }
}