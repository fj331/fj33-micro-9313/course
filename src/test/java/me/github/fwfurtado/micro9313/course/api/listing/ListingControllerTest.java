package me.github.fwfurtado.micro9313.course.api.listing;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import me.github.fwfurtado.micro9313.course.shared.Category;
import me.github.fwfurtado.micro9313.course.shared.Course;
import me.github.fwfurtado.micro9313.course.shared.Level;
import me.github.fwfurtado.micro9313.course.view.ChapterViewMapper;
import me.github.fwfurtado.micro9313.course.view.CourseDetailViewMapper;
import me.github.fwfurtado.micro9313.course.view.CourseViewMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class ListingControllerTest {

    @Mock
    private ListingRepository repository;


    @Spy
    private ChapterViewMapper chapterViewMapper;

    @BeforeEach
    void setup() {

        var service = new ListingService(repository,
                new CourseViewEntityFactory(new CourseViewMapper(), new CourseLinkFactory()),
                new CourseDetailViewEntityFactory(new CourseDetailViewMapper(chapterViewMapper),
                new CourseLinkFactory()));
        var course = new Course("Java Soa Integration", "Some description", Level.ADVANCED, Category.PROGRAMMING, "java-soa-integration");

        given(repository.findBySlug("java-soa-integration")).willReturn(Optional.of(course));

        RestAssuredMockMvc.standaloneSetup(new ListingController(service));
    }

}