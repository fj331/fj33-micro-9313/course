package me.github.fwfurtado.micro9313.course.api.chapters;

import me.github.fwfurtado.micro9313.course.infra.Mapper;
import me.github.fwfurtado.micro9313.course.shared.Chapter;
import org.springframework.stereotype.Component;

import static java.util.stream.Collectors.toList;

@Component
class ChapterFormMapper implements Mapper<ChapterForm, Chapter> {

    private final ChapterContentFormMapper contentMapper;

    ChapterFormMapper(ChapterContentFormMapper contentMapper) {
        this.contentMapper = contentMapper;
    }

    @Override
    public Chapter map(ChapterForm source) {
        var content = source.content().stream().map(contentMapper::map).collect(toList());

        return new Chapter(source.title(), content);
    }
}
