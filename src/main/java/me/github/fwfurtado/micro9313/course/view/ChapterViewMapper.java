package me.github.fwfurtado.micro9313.course.view;

import me.github.fwfurtado.micro9313.course.infra.Mapper;
import me.github.fwfurtado.micro9313.course.shared.Chapter;
import me.github.fwfurtado.micro9313.course.shared.ChapterContent;
import org.springframework.stereotype.Component;

import static java.util.stream.Collectors.toList;

@Component
public class ChapterViewMapper implements Mapper<Chapter, ChapterView> {
    @Override
    public ChapterView map(Chapter source) {
        var content = source.getContent().stream().map(ChapterContent::getName).collect(toList());
        return new ChapterView(source.getTitle(), content);
    }
}
