package me.github.fwfurtado.micro9313.course.infra;

public interface Mapper<S, T> {
    T map(S source);
}
