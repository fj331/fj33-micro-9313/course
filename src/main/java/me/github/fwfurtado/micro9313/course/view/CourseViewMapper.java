package me.github.fwfurtado.micro9313.course.view;

import me.github.fwfurtado.micro9313.course.infra.Mapper;
import me.github.fwfurtado.micro9313.course.shared.Course;
import org.springframework.stereotype.Component;

@Component
public class CourseViewMapper implements Mapper<Course, CourseView> {
    public CourseView map(Course course) {
        return new CourseView(course.getTitle(), course.getDescription(), course.getLevel(), course.getCategory());
    }
}
