package me.github.fwfurtado.micro9313.course.shared;

public enum Category {
    PROGRAMMING,
    BUSINESS,
    AGILE,
    INFRA
}
