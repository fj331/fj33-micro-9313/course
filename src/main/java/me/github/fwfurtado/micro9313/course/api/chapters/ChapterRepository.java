package me.github.fwfurtado.micro9313.course.api.chapters;

import me.github.fwfurtado.micro9313.course.shared.Course;

import java.util.Optional;

public interface ChapterRepository {

    Optional<Course> findBySlug(String slug);

    void save(Course course);
}
