package me.github.fwfurtado.micro9313.course.api.chapters;

import me.github.fwfurtado.micro9313.course.infra.Mapper;
import me.github.fwfurtado.micro9313.course.shared.ChapterContent;
import org.springframework.stereotype.Component;

@Component
class ChapterContentFormMapper implements Mapper<ChapterContentForm, ChapterContent> {
    @Override
    public ChapterContent map(ChapterContentForm source) {
        return new ChapterContent(source.name(), source.movieUrl());
    }
}
