package me.github.fwfurtado.micro9313.course.view;

import me.github.fwfurtado.micro9313.course.infra.Mapper;
import me.github.fwfurtado.micro9313.course.shared.Course;
import org.springframework.stereotype.Component;

import static java.util.stream.Collectors.toList;

@Component
public class CourseDetailViewMapper implements Mapper<Course, CourseDetailView> {

    private final ChapterViewMapper chapterViewMapper;

    public CourseDetailViewMapper(ChapterViewMapper chapterViewMapper) {
        this.chapterViewMapper = chapterViewMapper;
    }

    public CourseDetailView map(Course course) {

        var chapters = course.getChapters().stream().map(chapterViewMapper::map).collect(toList());

        return new CourseDetailView(
                course.getSlug(),
                course.getTitle(),
                course.getDescription(),
                course.getLevel(),
                course.getCategory(),
                chapters
                );
    }
}
