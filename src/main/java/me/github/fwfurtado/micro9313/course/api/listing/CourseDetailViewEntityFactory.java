package me.github.fwfurtado.micro9313.course.api.listing;

import me.github.fwfurtado.micro9313.course.shared.Course;
import me.github.fwfurtado.micro9313.course.view.CourseDetailView;
import me.github.fwfurtado.micro9313.course.view.CourseDetailViewMapper;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
class CourseDetailViewEntityFactory {
    private final CourseDetailViewMapper mapper;
    private final CourseLinkFactory linkFactory;

    CourseDetailViewEntityFactory(CourseDetailViewMapper mapper, CourseLinkFactory linkFactory) {
        this.mapper = mapper;
        this.linkFactory = linkFactory;
    }

    public EntityModel<CourseDetailView> factory(Course course) {
        var view = mapper.map(course);

        var links = linkFactory.factoryBy(course);

        return EntityModel.of(view, links);
    }
}
