package me.github.fwfurtado.micro9313.course.api.listing;

import me.github.fwfurtado.micro9313.course.infra.exceptions.ResourceNotFoundException;
import me.github.fwfurtado.micro9313.course.view.CourseDetailView;
import me.github.fwfurtado.micro9313.course.view.CourseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
class ListingService {
    private static final Logger LOG = LoggerFactory.getLogger(ListingService.class);
    private final ListingRepository repository;
    private final CourseViewEntityFactory entityFactory;
    private final CourseDetailViewEntityFactory detailEntityFactory;

    ListingService(ListingRepository repository, CourseViewEntityFactory entityFactory, CourseDetailViewEntityFactory detailEntityFactory) {
        this.repository = repository;
        this.entityFactory = entityFactory;
        this.detailEntityFactory = detailEntityFactory;
    }

    public List<EntityModel<CourseView>> listAllCourses() {
        return repository.findAll().map(entityFactory::factory).collect(Collectors.toList());
    }


    public EntityModel<CourseDetailView> showDetailsOf(String slug) {
        LOG.info("Receive Request!");
        return repository.findBySlug(slug).map(detailEntityFactory::factory)
                .orElseThrow(() -> new ResourceNotFoundException("Cannot find a course with slug " + slug));
    }
}
