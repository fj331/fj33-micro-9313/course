package me.github.fwfurtado.micro9313.course.api.listing;

import me.github.fwfurtado.micro9313.course.shared.Course;
import me.github.fwfurtado.micro9313.course.view.CourseView;
import me.github.fwfurtado.micro9313.course.view.CourseViewMapper;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Links;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
class CourseViewEntityFactory {
    private final CourseViewMapper mapper;
    private final CourseLinkFactory linkFactory;

    CourseViewEntityFactory(CourseViewMapper mapper, CourseLinkFactory linkFactory) {
        this.mapper = mapper;
        this.linkFactory = linkFactory;
    }

    public EntityModel<CourseView> factory(Course course) {
        var view = mapper.map(course);

        var links = linkFactory.factoryBy(course);

        return EntityModel.of(view, links);
    }
}
