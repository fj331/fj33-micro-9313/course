package me.github.fwfurtado.micro9313.course.api.chapters;

import me.github.fwfurtado.micro9313.course.api.CourseController;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CourseController
public interface ChapterApi {

    @PutMapping("{slug}")
    ResponseEntity<?> addChapter(@PathVariable String slug, @RequestBody @Validated ChapterForm form);

}
