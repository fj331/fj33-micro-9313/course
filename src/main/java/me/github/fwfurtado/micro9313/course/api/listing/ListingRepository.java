package me.github.fwfurtado.micro9313.course.api.listing;

import me.github.fwfurtado.micro9313.course.shared.Course;
import org.springframework.data.jpa.repository.EntityGraph;

import java.util.Optional;
import java.util.stream.Stream;

public interface ListingRepository {
    Stream<Course> findAll();

    Optional<Course> findBySlug(String slug);
}
