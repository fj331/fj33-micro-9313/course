package me.github.fwfurtado.micro9313.course.shared;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "courses")
@NamedEntityGraphs(
        @NamedEntityGraph(name = "courses-with-chapters", attributeNodes = @NamedAttributeNode("chapters"))
)
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Category category;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Level level;

    @NotNull
    private String title;

    @NotNull
    private String description;

    @NotNull
    private String slug;

    @OneToMany(cascade = CascadeType.MERGE)
    private List<Chapter> chapters = new ArrayList<>();

    protected Course() {
    }

    public Course(String title, String description, Level level, Category category, String slug) {
        this.title = title;
        this.description = description;
        this.level = level;
        this.category = category;
        this.slug = slug;
    }

    public Long getId() {
        return id;
    }

    public Level getLevel() {
        return level;
    }

    public Category getCategory() {
        return category;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getSlug() {
        return slug;
    }

    public List<Chapter> getChapters() {
        return chapters;
    }

    public void addChapter(Chapter chapter) {
        chapters.add(chapter);
    }
}
