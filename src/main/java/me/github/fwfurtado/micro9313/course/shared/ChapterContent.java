package me.github.fwfurtado.micro9313.course.shared;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.net.URI;

@Embeddable
public class ChapterContent {
    @NotNull
    private String name;

    @NotNull
    private URI movie;

    protected ChapterContent() {
    }

    public ChapterContent(String name, URI movie) {
        this.name = name;
        this.movie = movie;
    }

    public String getName() {
        return name;
    }

    public URI getMovie() {
        return movie;
    }
}
