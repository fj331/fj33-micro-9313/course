package me.github.fwfurtado.micro9313.course.infra;

import me.github.fwfurtado.micro9313.course.api.chapters.ChapterRepository;
import me.github.fwfurtado.micro9313.course.api.listing.ListingRepository;
import me.github.fwfurtado.micro9313.course.api.register.RegistrationRepository;
import me.github.fwfurtado.micro9313.course.shared.Course;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.Repository;

import java.util.Optional;

interface CourseRepository extends Repository<Course, Long>, RegistrationRepository, ListingRepository, ChapterRepository {

    @EntityGraph("courses-with-chapters")
    Optional<Course> findBySlug(String slug);

}
