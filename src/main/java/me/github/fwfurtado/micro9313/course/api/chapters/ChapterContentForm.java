package me.github.fwfurtado.micro9313.course.api.chapters;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.net.URI;

record ChapterContentForm(String name, @JsonProperty("movie_url") URI movieUrl) {

}
