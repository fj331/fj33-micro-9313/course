package me.github.fwfurtado.micro9313.course.api.chapters;

import java.util.List;

record ChapterForm(String title, List<ChapterContentForm> content) {
}
