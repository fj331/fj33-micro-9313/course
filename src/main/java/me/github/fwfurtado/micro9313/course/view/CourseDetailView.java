package me.github.fwfurtado.micro9313.course.view;

import com.fasterxml.jackson.annotation.JsonProperty;
import me.github.fwfurtado.micro9313.course.shared.Category;
import me.github.fwfurtado.micro9313.course.shared.Level;

import java.util.List;

public record CourseDetailView(
                               @JsonProperty("id")String slug,
                               @JsonProperty("title")String title,
                               @JsonProperty("description")String description,
                               @JsonProperty("level")Level level,
                               @JsonProperty("category")Category category,
                               @JsonProperty("content")List<ChapterView>content
) {
}