package me.github.fwfurtado.micro9313.course.api.chapters;

import me.github.fwfurtado.micro9313.course.infra.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
class ChapterService {
    private final ChapterRepository repository;
    private final ChapterFormMapper mapper;

    ChapterService(ChapterRepository repository, ChapterFormMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    public void addChapterBy(String slug, ChapterForm form) {

        var course = repository.findBySlug(slug)
                .orElseThrow(() -> new ResourceNotFoundException("Cannot find a course with slug " + slug));

        var chapter = mapper.map(form);

        course.addChapter(chapter);

        repository.save(course);
    }
}
