package me.github.fwfurtado.micro9313.course.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@RestController
@RequestMapping("courses")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CourseController {
}
