package me.github.fwfurtado.micro9313.course.api.register;

import me.github.fwfurtado.micro9313.course.shared.Course;

public interface RegistrationRepository {
    void save(Course course);

    boolean existsBySlug(String slug);
}
