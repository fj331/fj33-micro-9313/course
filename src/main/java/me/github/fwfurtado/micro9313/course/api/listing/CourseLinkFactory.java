package me.github.fwfurtado.micro9313.course.api.listing;

import me.github.fwfurtado.micro9313.course.api.chapters.ChapterApi;
import me.github.fwfurtado.micro9313.course.shared.Course;
import org.springframework.hateoas.Links;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
class CourseLinkFactory {

    Links factoryBy(Course course) {
        return Links.of(
                linkTo(methodOn(ListingController.class).showDetails(course.getSlug())).withRel("details"),
                linkTo(methodOn(ChapterApi.class).addChapter(course.getSlug(), null)).withRel("add-chapter")
        );
    }

}
