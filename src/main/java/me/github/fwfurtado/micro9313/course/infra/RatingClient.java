package me.github.fwfurtado.micro9313.course.infra;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "rating")
public interface RatingClient {

    @GetMapping("ratings/{id}")
    ResponseEntity<?> ratingsOfCourse(@PathVariable Long id);

}
