package me.github.fwfurtado.micro9313.course.api.listing;

import me.github.fwfurtado.micro9313.course.api.CourseController;
import me.github.fwfurtado.micro9313.course.view.CourseDetailView;
import me.github.fwfurtado.micro9313.course.view.CourseView;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@CourseController
public interface ListingApi {
    @GetMapping
    List<EntityModel<CourseView>> list();

    @GetMapping("{slug}")
    EntityModel<CourseDetailView> showDetails(@PathVariable String slug);
}
