package me.github.fwfurtado.micro9313.course.shared;

public enum Level {
    BEGINNER,
    INTERMEDIATE,
    ADVANCED
}
