package me.github.fwfurtado.micro9313.course.api.register;

import me.github.fwfurtado.micro9313.course.infra.SlugGenerator;
import me.github.fwfurtado.micro9313.course.shared.Course;
import org.springframework.stereotype.Component;

@Component
class CourseFactory {
    private final SlugGenerator slugGenerator;

    CourseFactory(SlugGenerator slugGenerator) {
        this.slugGenerator = slugGenerator;
    }

    public Course factoryBy(BasicCourseForm form) {
        var slug = slugGenerator.generateOf(form.title());

        return new Course(form.title(), form.description(), form.level(), form.category(), slug);
    }
}
