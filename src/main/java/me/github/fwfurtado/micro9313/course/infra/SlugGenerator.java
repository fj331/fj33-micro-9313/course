package me.github.fwfurtado.micro9313.course.infra;

import org.springframework.stereotype.Component;

import java.text.Normalizer;

@Component
public class SlugGenerator {
    public String generateOf(String text) {
        return  Normalizer.normalize(text.toLowerCase(), Normalizer.Form.NFD).replace(" ", "-");
    }
}
