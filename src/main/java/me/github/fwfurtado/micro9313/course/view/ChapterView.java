package me.github.fwfurtado.micro9313.course.view;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record ChapterView(@JsonProperty("title") String title, @JsonProperty("content") List<String> content) {
}
