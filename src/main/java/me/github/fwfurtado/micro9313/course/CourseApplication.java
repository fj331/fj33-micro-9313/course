package me.github.fwfurtado.micro9313.course;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.hateoas.config.EnableHypermediaSupport;

@EnableDiscoveryClient
@SpringBootApplication
@EnableHypermediaSupport(type= EnableHypermediaSupport.HypermediaType.HAL_FORMS)
public class CourseApplication {

    public static void main(String[] args) {
        SpringApplication.run(CourseApplication.class, args);
    }

}
