package me.github.fwfurtado.micro9313.course.api.register;

import me.github.fwfurtado.micro9313.course.api.CourseController;
import me.github.fwfurtado.micro9313.course.api.listing.ListingApi;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder.on;

@CourseController
class RegistrationCourseController {

    private final RegistrationService service;

    RegistrationCourseController(RegistrationService service) {
        this.service = service;
    }

    @PostMapping
    ResponseEntity<?> registerANewCourseBy(@RequestBody BasicCourseForm form, UriComponentsBuilder uriBuilder) {
        var slug = service.registerBy(form);

        var uri = MvcUriComponentsBuilder.relativeTo(uriBuilder).withMethodCall(on(ListingApi.class).showDetails(slug)).build().toUri();

        return created(uri).build();
    }


}
