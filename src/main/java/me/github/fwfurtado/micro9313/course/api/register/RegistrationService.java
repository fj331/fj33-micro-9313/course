package me.github.fwfurtado.micro9313.course.api.register;

import org.springframework.stereotype.Service;

@Service
class RegistrationService {
    private final CourseFactory courseFactory;
    private final RegistrationRepository repository;

    RegistrationService(CourseFactory courseFactory, RegistrationRepository repository) {
        this.courseFactory = courseFactory;
        this.repository = repository;
    }

    public String registerBy(BasicCourseForm form) {
        var course = courseFactory.factoryBy(form);
        var generatedSlug = course.getSlug();

        if (repository.existsBySlug(generatedSlug)) {
            throw new IllegalArgumentException("Already exists slug " + generatedSlug);
        }

        repository.save(course);

        return generatedSlug;
    }
}
