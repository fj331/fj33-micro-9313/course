package me.github.fwfurtado.micro9313.course.api.register;

import me.github.fwfurtado.micro9313.course.shared.Category;
import me.github.fwfurtado.micro9313.course.shared.Level;

record BasicCourseForm(String title, String description, Level level, Category category) {}
