package me.github.fwfurtado.micro9313.course.infra.exceptions;

public class ResourceNotFoundException extends IllegalArgumentException {
    public ResourceNotFoundException(String message) {
        super(message);
    }
}
