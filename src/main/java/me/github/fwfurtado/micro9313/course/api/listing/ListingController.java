package me.github.fwfurtado.micro9313.course.api.listing;

import me.github.fwfurtado.micro9313.course.api.CourseController;
import me.github.fwfurtado.micro9313.course.view.CourseDetailView;
import me.github.fwfurtado.micro9313.course.view.CourseView;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@CourseController
class ListingController implements ListingApi {

    private final ListingService service;

    ListingController(ListingService service) {
        this.service = service;
    }

    @Override
    public List<EntityModel<CourseView>> list() {
        return service.listAllCourses();
    }

    @Override
    public EntityModel<CourseDetailView> showDetails(@PathVariable String slug) {
        return service.showDetailsOf(slug);
    }
}
