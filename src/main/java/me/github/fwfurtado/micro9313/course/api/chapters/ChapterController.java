package me.github.fwfurtado.micro9313.course.api.chapters;

import me.github.fwfurtado.micro9313.course.api.CourseController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

@CourseController
class ChapterController implements ChapterApi {
    private final ChapterService service;

    ChapterController(ChapterService service) {
        this.service = service;
    }

    @Override
    public ResponseEntity<?> addChapter(String slug, ChapterForm form) {
        service.addChapterBy(slug, form);

        return ResponseEntity.noContent().build();
    }
}
